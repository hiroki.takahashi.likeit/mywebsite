<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="jp">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
    <link rel="stylesheet" href="css/style.css" type="text/css" />
    <title>ワイン更新</title>
</head>
<body>

    <!-- ヘッダー -->
    <jsp:include page="header.jsp" flush="true" />

    <!-- コンテンツ -->
    <main>
        <div class="content-left">
            <p>左サイドコンテンツ</p>
        </div>
        <div class="v_line_left">
            <!-- some<br> other<br> content<br> -->
        </div>
        <div class="content-main">
            <p>コンテンツ</p>
        </div>
    </main>

</body>
</html>