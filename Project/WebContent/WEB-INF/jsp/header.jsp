<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<header>
	<div class="header-inner-title">
		<a href="index">ワインレビュー</a>
	</div>

	<c:if test="${loginUserInfo.id == null}" var="loginSessionFlg" />

	<div class="header-inner-nav">
		<ul>
			<c:if test="${loginSessionFlg}">
				<li><a href="login">Log in</a></li>
			</c:if>
			<c:if test="${!loginSessionFlg}">
				<li><a href="mypage">My Page</a></li>
				<li><a href="logout">Log Out</a></li>
			</c:if>
		</ul>
	</div>
</header>