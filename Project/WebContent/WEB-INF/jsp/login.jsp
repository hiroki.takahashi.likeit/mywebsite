<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="jp">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
    <link rel="stylesheet" href="css/style.css" type="text/css" />
    <link rel="stylesheet" href="css/login.css" type="text/css" />
    <title>login</title>
</head>
<body>

    <!-- ヘッダー -->
    <jsp:include page="header.jsp" flush="true" />

    <!-- コンテンツ -->
    <main>
        <form class="login-form mx-auto" action="login" method="post">
            <div class="input_area">
                <div class="group">
                    <label for="loginId font">ログインID</label>
                    <input class="loginId input_box" id="loginId" type="text" name="loginId">
                    <div class="text_underline"></div>
                </div>
                <div class="group">
                    <label for="passwd font">パスワード</label>
                    <input class="passwd input_box" id="passwd" type="password" name="passwd">
                    <div class="text_underline"></div>
                </div>
            </div>
            <div class="button_area">
                <div class="create_Btn">
                    <a href="userCreate">+新規登録</a>
                </div>
                <div class="login_Btn">
                    <button class="btn btn-primary" type="submit">ログイン</button>
                </div>
            </div>

        </form>
    </main>

</body>
</html>