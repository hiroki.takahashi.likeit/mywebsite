<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="jp">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
    <link rel="stylesheet" href="css/style.css" type="text/css" />
    <link rel="stylesheet" href="css/userCreate.css" type="text/css" />
    <title>新規ユーザ登録</title>
</head>
<body>

    <!-- ヘッダー -->
    <jsp:include page="header.jsp" flush="true" />

    <!-- コンテンツ -->
    <main>
        <form class="create-form mx-auto" action="userCreate" method="post">
            <div class="input_area">
                <div class="group">
                    <label for="input-LoginId font">ログインID</label>
                    <input class="input-LoginId input_box" id="input-LoginId" name="input-LoginId" type="text">
                    <div class="text_underline"></div>
                </div>
                <div class="group">
                    <label for="input-Passwd font">パスワード</label>
                    <input class="input-Passwd input_box" id="input-Passwd" name="input-Passwd" type="password">
                    <div class="text_underline"></div>
                </div>
                <div class="group">
                    <label for="retype-Passwd font">パスワード(再入力)</label>
                    <input class="retype-Passwd input_box" id="retype-Passwd" name="retype-Passwd" type="password">
                    <div class="text_underline"></div>
                </div>
                <div class="group">
                    <label for="input-NickName font">ニックネーム(任意)</label>
                    <input class="input-NickName input_box" id="input-NickName" name="input-NickName" type="text">
                    <div class="text_underline"></div>
                </div>
                <div class="group">
                    <label for="input-BirthDate font">生年月日</label>
                    <input class="input-BirthDate input_box" id="input-BirthDate" name="input-BirthDate" type="date">
                    <div class="text_underline"></div>
                </div>
            </div>
            <div class="button_area">
                <div class="login_Btn">
                    <button class="btn btn-primary" type="submit">登録</button>
                </div>
            </div>

        </form>
    </main>

</body>
</html>