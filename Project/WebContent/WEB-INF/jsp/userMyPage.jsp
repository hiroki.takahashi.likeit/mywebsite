<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="jp">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
    <link rel="stylesheet" href="css/style.css" type="text/css" />
    <title>Myページ</title>
</head>
<body>

    <!-- ヘッダー -->
    <jsp:include page="header.jsp" flush="true" />

    <!-- コンテンツ -->
    <main>
        <div class="content-left">
            <p></p>
            <form class="serch_form" action="mypage" method="GET">

                <div class="form-group row">
                    <label for="wine_color" class="col-sm-3 col-form-label">検索</label>
                    <input type="text" name="serch_word" id="serch_word" placeholder="フリーワード">
                </div>

                <div class="form-group row">
                    <label for="country" class="col-sm-3 col-form-label">生産地</label>
                    <select name="country" id="country">
                        <option value=""></option>
                        <c:forEach var="country" items="${countryList}">
                        	<option value="${country.id}">${country.countryName}</option>
                        </c:forEach>
                    </select>
                </div>
                <div class="form-group row">
                    <label for="wine_color" class="col-sm-3 col-form-label">色</label>
                    <select name="wine_color" id="wine_color">
                        <option value=""></option>
                        <c:forEach var="colorType" items="${colorTypeList}">
	                        <option value="${colorType.id}">${colorType.colorType}</option>
                        </c:forEach>
                    </select>
                </div>
                <div class="form-group row">
                    <label for="wine_type" class="col-sm-3 col-form-label">味</label>
                    <select name="wine_type" id="wine_type">
                        <option value=""></option>
                        <c:forEach var="tasteType" items="${tasteTypeList}">
	                        <option value="${tasteType.id}">${tasteType.tasteType}</option>
                        </c:forEach>
                    </select>
                </div>
                <div class="text-right" style="margin-right: 30px;">
                    <button class="btn btn-primary">検索</button>
                </div>
            </form>
        </div>
        <div class="v_line_left">
            <!-- some<br> other<br> content<br> -->
        </div>
        <div class="content-main">
            <div class="userDetail">
                <h1 style="font-size: larger;">ユーザ情報</h1>
                <form action="mypage" method="POST">
                    <div>
                        <label>ログインID</label><input class="input_box" name="myLoginId" type="text" value="${loginUserInfo.loginId}">
                        <div class="text_underline"></div>
                    </div>
                    <div>
                        <label>パスワード</label><input class="input_box" name="myPasswd" type="password">
                        <div class="text_underline"></div>
                    </div>
                    <div>
                        <label>パスワード(再確認)</label><input class="input_box" name="myRePasswd" type="password">
                        <div class="text_underline"></div>
                    </div>
                    <div>
                        <label>ニックネーム</label><input class="input_box" name="myNickName" type="text" value="${loginUserInfo.nickName}">
                        <div class="text_underline"></div>
                    </div>
                    <div class="update_Btn text-right">
                        <button class="btn btn-danger" type="submit">更新</button>
                    </div>
                </form>
            </div>
            <div class="form-group row">
                <h1 class="font col-sm-10" style="margin-bottom: 30px">${loginUserInfo.nickName} さんのレビュー</h1>
                <div class="create_wine text-right">
                    <a href="wineCreate.html" class="btn btn-danger" style="margin-top: 5px;">ワイン新規登録</a>
                </div>
            </div>
            <div class="reviwe">
                <ul>
                <c:forEach var="wine" items="${myWineList}"  begin="${recordsOffset}" end="${recordsPerPage + recordsOffset - 1}">
                    <li class="rewiew_list">
                        <a href="wineDetail?wid=${wine.id}"><img class="wine_img" src="${wine.wineImagePath}" alt=""></a>
                        <div>
                            <a href="wineDetail?wid=${wine.id}"><h2 class="font" style="margin-top: 10px; margin-bottom: 10px;">${wine.wineName}</h2></a>
                            <h5>　- 生産地 :${wine.wineCountry}</h5>
                            <h5>　- 年　　 :${wine.wineCreateYear}</h5>
                            <h5>　- 色　　 :${wine.wineColor}</h5>
                            <h5>　- 味　　 :${wine.wineTaste}</h5>
                        </div>
                    </li>
				</c:forEach>
                </ul>
            </div>
            <div class="page">
                <ul class="page_list">
                    <c:choose>
						<c:when test="${pageNum == 1}">
							<li class="disabled"><a>前</a></li>
						</c:when>
						<c:otherwise>
							<li class="waves-effect"><a href="mypage?search_word=${searchWord}&page=${pageNum - 1}">前</a></li>
						</c:otherwise>
					</c:choose>

                    <c:forEach var="i" begin="1" end="${maxPageSize}" step="1">
	                    <li><a href="mypage?page=${i}">${i}</a></li>
                    </c:forEach>

       				<c:choose>
						<c:when test="${pageNum == pageMax || pageMax == 0}">
							<li class="disabled"><a><i class="material-icons">後</i></a></li>
						</c:when>
						<c:otherwise>
							<li class="waves-effect"><a href="mypage?search_word=${searchWord}&page=${pageNum + 1}">次</a></li>
						</c:otherwise>
					</c:choose>
                </ul>
            </div>
        </div>
    </main>

</body>
</html>