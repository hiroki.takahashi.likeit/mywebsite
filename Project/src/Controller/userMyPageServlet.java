package Controller;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.SearchDao;
import dao.UserDao;
import dao.WineDao;
import model.ColorType;
import model.Country;
import model.TasteType;
import model.User;
import model.Wine;
import model.common;

/**
 * Servlet implementation class userMyPageServlet
 */
@WebServlet("/mypage")
public class userMyPageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public userMyPageServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		// 変数の初期化
		int myWineListSize = 0;
		int pageNum = 1;
		int recordsPerPage = 1;
		int maxPageSize = 1;
		int pageOffset = 0;
		int recordsOffset = 0;

		// ログインユーザ情報の取得
		HttpSession session = request.getSession();
		User loginUser = (User)session.getAttribute("loginUserInfo");

		// セッションがなければログイン画面へ移動する



		// ページ数の取得
		if(request.getParameter("page")==null || request.getParameter("page").equals("1")) {
			pageNum = 1;
		} else {
			pageNum = Integer.parseInt(request.getParameter("page"));
		}

		// 検索ワードの取得

		// 検索用リストの取得
		SearchDao searchDao = new SearchDao();
		List<Country> countryList = searchDao.getCountryAll();
		List<TasteType> tasteTypeList = searchDao.getTasteTypeAll();
		List<ColorType> colorTypeList = searchDao.getColorTypeAll();

		// ログインユーザの登録したワインのリストを取得
		WineDao wineDao = new WineDao();
		List<Wine> myWineList = wineDao.findWaineById(loginUser.getId());

		myWineListSize = myWineList.size();

		// ページ数計算
		maxPageSize = (int) Math.ceil((double) myWineListSize / recordsPerPage);

		if(pageNum == 1){
			System.out.println("Top page View");
		} else {
			pageOffset = pageNum - 1;
			recordsOffset = pageOffset * recordsPerPage;
			System.out.println(request.getParameter("page") + " page View");
			System.out.println("Offset:" + recordsOffset);
		}

		request.setAttribute("countryList", countryList);
		request.setAttribute("tasteTypeList", tasteTypeList);
		request.setAttribute("colorTypeList", colorTypeList);
		request.setAttribute("myWineList", myWineList);
		request.setAttribute("myWineListSize", myWineListSize);
		request.setAttribute("recordsPerPage", recordsPerPage);
		request.setAttribute("pageNum", pageNum);
		request.setAttribute("maxPageSize", maxPageSize);
		request.setAttribute("recordsOffset", recordsOffset);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userMyPage.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		//ユーザ情報の更新

		// 文字コード設定
		request.setCharacterEncoding("UTF-8");

		// 更新対象のユーザ情報取得
		HttpSession session = request.getSession();
		User updateUserDetail = (User)session.getAttribute("loginUserInfo");


		int updateIndex = 0;

		// フォーム上のデータを取得
		String loginId = request.getParameter("myLoginId");
		String password = request.getParameter("myPasswd");
		String rePassword = request.getParameter("myRePasswd");
		String nickName = request.getParameter("myNickName");

		// 作成日時、更新日時を現在時刻で取得
		Timestamp updateDate = new Timestamp(System.currentTimeMillis());

		// 登録データの整合性チェック用
		boolean integrityFlg = true;

		// フォーム未入力チェック
		String[] params = {loginId, password, rePassword, nickName};
		for (String values : params) {
			if(values.isEmpty()) {
				integrityFlg = false;
				System.out.println("NG_flg:フォーム未入力");
				break;
			}
		}

		// パスワード一致確認
		if(!(password.equals(rePassword))) {
			integrityFlg = false;
			System.out.println("NG_flg:確認パスワード不一致");
		}

		// 整合性フラグチェック
		if (integrityFlg == true) {
			// OK:ユーザの登録処理
			// パスワードの暗号化
			String result = common.encryptForPassword(password);

			// 更新:パスワード、名前、誕生日、更新日時
			updateUserDetail.setLoginId(loginId);
			updateUserDetail.setPassword(result);
			updateUserDetail.setnickName(nickName);
			updateUserDetail.setUpdateDate(updateDate);

			// Doa 呼び出し
			UserDao userDao = new UserDao();
			updateIndex = userDao.updateUser(updateUserDetail);

		}

		// アップデートユーザのセッション情報を破棄
		session.removeAttribute("updateUserDetail");

		// 成否チェック
		if (updateIndex > 0) {
			// OK
			doGet(request, response);
		} else if(updateIndex == 0) {
			// NG:登録画面を再表示
			// 失敗メッセージ
			request.setAttribute("statusMsg", "入力された内容は正しくありません");

			// 更新画面再表示
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);
		}
	}
}
