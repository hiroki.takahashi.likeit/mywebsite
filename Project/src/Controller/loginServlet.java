package Controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;
import model.common;

/**
 * Servlet implementation class loginServlet
 */
@WebServlet("/login")
public class loginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public loginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		// フォーム上のID,Passの取得
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("passwd");

		// パスワードの暗号化解決
		String result = common.encryptForPassword(password);

		System.out.println("Login ID:" + loginId);
		System.out.println("Password:" + result);

		// DB接続,ユーザー取得
		UserDao userDao = new UserDao();
		User loginUser = userDao.findByLoginInfo(loginId, result);


		if (loginUser == null) {
			// 見つからなかったとき
//			request.setAttribute("statusMsg", "ログインIDまたはパスワードが異なります");
			System.out.println("Not Find Login User");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
		} else {
			// 見つかった時
			// ログインセッション
			System.out.println("Login Successful");
			HttpSession session = request.getSession();
			session.setAttribute("loginUserInfo", loginUser);

			// ページ移動
			response.sendRedirect("index");
		}
	}

}
