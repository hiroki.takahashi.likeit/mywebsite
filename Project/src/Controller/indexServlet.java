package Controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.SearchDao;
import dao.WineDao;
import model.ColorType;
import model.Country;
import model.TasteType;
import model.Wine;

/**
 * Servlet implementation class indexServlet
 */
@WebServlet("/index")
public class indexServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public indexServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 * 1:リスト取得
	 * 2:ページング処理
	 * 3:リスト操作
	 * 4:リクエストデータセット
	 * 5:フォワード
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		// 変数の初期化
		int wineListSize = 0;
		int pageNum = 1;
		int recordsPerPage = 2;
		int maxPageSize = 1;
		int pageOffset = 0;
		int recordsOffset = 0;

		// 検索用リストの取得
		SearchDao searchDao = new SearchDao();
		List<Country> countryList = searchDao.getCountryAll();
		List<TasteType> tasteTypeList = searchDao.getTasteTypeAll();
		List<ColorType> colorTypeList = searchDao.getColorTypeAll();

		// ページ数の取得
		if(request.getParameter("page")==null || request.getParameter("page").equals("1")) {
			pageNum = 1;
		} else {
			pageNum = Integer.parseInt(request.getParameter("page"));
		}

		// ワインリストの取得
		WineDao wineDao = new WineDao();
		List<Wine> wineList = wineDao.findAll();

		wineListSize = wineList.size();

		// ページ数計算
		maxPageSize = (int) Math.ceil((double) wineListSize / recordsPerPage);

		if(pageNum == 1){
			System.out.println("Top page View");
		} else {
			pageOffset = pageNum - 1;
			recordsOffset = pageOffset * recordsPerPage;
			System.out.println(request.getParameter("page") + " page View");
			System.out.println("Offset:" + recordsOffset);
		}

		request.setAttribute("countryList", countryList);
		request.setAttribute("tasteTypeList", tasteTypeList);
		request.setAttribute("colorTypeList", colorTypeList);
		request.setAttribute("wineList", wineList);
		request.setAttribute("wineListSize", wineListSize);
		request.setAttribute("recordsPerPage", recordsPerPage);
		request.setAttribute("pageNum", pageNum);
		request.setAttribute("maxPageSize", maxPageSize);
		request.setAttribute("recordsOffset", recordsOffset);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
