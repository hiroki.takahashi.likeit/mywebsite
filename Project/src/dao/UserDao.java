package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
//import java.util.Date;
import java.util.List;

import model.User;
import model.common;

public class UserDao {

	/**
	 * ログインユーザの検索
	 */
	public User findByLoginInfo(String loginId, String password) {
		// 変数初期化
		Connection conn = null;

		try {
			// コネクション確立
			conn = DBManager.getConnection();

			// SQL文
			String sql = "SELECT * FROM t_user WHERE login_id = ? and password = ?";

			// パラメータ設定
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, password);
			// 実行
			ResultSet rs = pStmt.executeQuery();

			// 一回だけ実行
			if (!rs.next()) {
				return null;
			}

			// ヒットしたデータをインスタンスにセット
			int userId = rs.getInt("id");
			String loginIdStr = rs.getString("login_id");
			String NicknameStr = rs.getString("User_Nickname");
			return new User(userId, loginIdStr, NicknameStr);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}


	/**
	 * 全ユーザを検索してユーザ型のリストとして返す
	 * adminユーザは対象から除外
	 */
	public List<User> findAll() {
		// 変数初期化
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE login_id not in ('admin')";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String password = rs.getString("password");
				String nickName = rs.getString("nickName");
				Date birthDate = rs.getDate("user_birth_date");
				Timestamp createDate = rs.getTimestamp("user_create_date");
				Timestamp updateDate = rs.getTimestamp("user_update_date");

				User user = new User(id, loginId, password, nickName, birthDate, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	/**
	 * ユーザ検索用
	 * ログインID、名前、生年月日からヒットしたユーザのリストを返す
	 * admin除外
	 */
	public List<User> findUser(String targetLoginId,
								String targetName,
								java.util.Date birthDateA,
								java.util.Date birthDateZ){

		// 変数初期化
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			// コネクション確立
			conn = DBManager.getConnection();

			// SQL文
			String sql = "SELECT * FROM user WHERE login_id not in ('admin') "
					+ "AND CASE WHEN ? = '' THEN '' ELSE login_id END = ? "	// 入力有り:完全一致、無し:全検索
					+ "AND name LIKE ? "									// 部分一致検索
					+ "AND birth_date BETWEEN ? AND ?";						// 範囲検索

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, targetLoginId);
			pStmt.setString(2, targetLoginId);
			pStmt.setString(3, "%" + targetName + "%");
			pStmt.setDate(4, common.convUtilDate(birthDateA));
			pStmt.setDate(5, common.convUtilDate(birthDateZ));

			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String password = rs.getString("password");
				String nickName = rs.getString("nickName");
				Date birthDate = rs.getDate("user_birth_date");
				Timestamp createDate = rs.getTimestamp("user_create_date");
				Timestamp updateDate = rs.getTimestamp("user_update_date");

				User user = new User(id, loginId, password, nickName, birthDate, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	/**
	 * 与えられたカラムで完全一致するユーザを検索し返す
	 */
	public User findBy(String targetColumn, String targetData) {
		Connection conn = null;
		try {
			// DB接続
			conn = DBManager.getConnection();

			// SQL文
			String sql = "SELECT * FROM user WHERE " + targetColumn + " = ?";

			// SQL実行＆結果の取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, targetData);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			int id = rs.getInt("id");
			String loginId = rs.getString("login_id");
			String password = rs.getString("password");
			String nickName = rs.getString("nickName");
			Date birthDate = rs.getDate("user_birth_date");
			Timestamp createDate = rs.getTimestamp("user_create_date");
			Timestamp updateDate = rs.getTimestamp("user_update_date");

			User user = new User(id, loginId, password, nickName, birthDate, createDate, updateDate);

			return user;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
	/**
	 * ユーザIdから一致するユーザを検索し返す
	 */
	public User findById(String targetId) {
		System.out.println("Start findById:TargetID=" + targetId);
		Connection conn = null;
		try {
			// DB接続
			conn = DBManager.getConnection();

			// SQL文
			String sql = "SELECT * FROM t_user WHERE id = ?";

			// SQL実行＆結果の取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, targetId);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			String loginId = rs.getString("login_id");
			String password = rs.getString("password");
			String nickName = rs.getString("user_nickName");
			Date birthDate = rs.getDate("user_birth_date");
			Timestamp createDate = rs.getTimestamp("user_create_date");
			Timestamp updateDate = rs.getTimestamp("user_update_date");

			User user = new User(loginId, password, nickName, birthDate, createDate, updateDate);

			return user;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	/**
	 * ユーザ新規登録用
	 * 与えられたユーザ情報をDBへ登録する
	 */
	public int createUser(User targetUser) {
		Connection conn = null;
		try {
			// DB接続
			conn = DBManager.getConnection();

			// SQL文
			String sql =  "INSERT INTO "
							+ "t_user("
								+ "login_id, "
								+ "password, "
								+ "user_nickname, "
								+ "user_birth_date, "
								+ "user_create_date, "
								+ "user_update_date) "
							+ "VALUE(?, ?, ?, ?, ?, ?)";


			// SQL実行＆結果の取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, targetUser.getLoginId());
			pStmt.setString(2, targetUser.getPassword());
			pStmt.setString(3, targetUser.getnickName());
			pStmt.setDate(4, common.convUtilDate(targetUser.getBirthDate()));
			pStmt.setTimestamp(5, targetUser.getCreateDate());
			pStmt.setTimestamp(6, targetUser.getUpdateDate());
			int rs = pStmt.executeUpdate();

			return rs;

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

	/**
	 * ユーザ情報更新用
	 * 与えられたユーザのIDに一致するユーザ情報を更新
	 */
	public int updateUser(User targetUser) {
		Connection conn = null;
		try {
			// DB接続
			conn = DBManager.getConnection();

			// SQL文
			String sql = "UPDATE t_user SET "
					+ "login_id = ?,"
					+ "password = ?,"
					+ "user_nickname = ?,"
					+ "user_update_date = ?"
					+ " WHERE id = ?";

			// SQL実行＆結果の取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, targetUser.getLoginId());
			pStmt.setString(2, targetUser.getPassword());
			pStmt.setString(3, targetUser.getnickName());
			pStmt.setTimestamp(4, targetUser.getUpdateDate());
			pStmt.setInt(5, targetUser.getId());

			int updateIndex = pStmt.executeUpdate();

			//--終了--
			return updateIndex;

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;

		} finally {

			// DB切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}
			}
		}
	}

	/**
	 * ユーザ削除用
	 * 与えられたユーザのIDに一致するユーザ情報を削除
	 */
	public int deleteUser(User targetUser) {
		Connection conn = null;
		try {
			// DB接続
			conn = DBManager.getConnection();

			// SQL文
			String sql = "DELETE FROM user WHERE id = ?";

			// SQL実行＆結果の取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, targetUser.getId());
			int deleteIndex = pStmt.executeUpdate();

			return deleteIndex;

			//--終了--

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;

		} finally {

			// DB切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}
			}
		}
	}
}