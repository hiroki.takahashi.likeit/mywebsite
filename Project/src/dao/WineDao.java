package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import model.Wine;

public class WineDao {

	/**
	 * 登録されているワイン情報一覧を
	 * ワイン型のリストとして返す
	 */
	public List<Wine> findAll() {

		// 変数初期化
		Connection conn = null;
		List<Wine> wineList = new ArrayList<Wine>();

		try {
			conn = DBManager.getConnection();

			String sql = "SELECT"
							+ " t_wine_detail.id,"
							+ " t_wine_detail.wine_name,"
							+ " t_wine_detail.wine_create_year,"
							+ " t_wine_detail.wine_grape_name,"
							+ " t_wine_detail.wine_body_score,"
							+ " t_wine_detail.wine_tannin_score,"
							+ " t_wine_detail.wine_memo,"
							+ " t_wine_detail.wine_image_path,"
							+ " t_wine_detail.wine_create_date,"
							+ " t_wine_detail.wine_update_date,"
							+ " m_wine_country.w_country_name,"
							+ " m_wine_color_type.w_color_type,"
							+ " m_wine_taste_type.w_taste_type "
						+ "FROM"
							+ " t_wine_detail "
						+ "INNER JOIN"
							+ " m_wine_country "
						+ "ON"
							+ " t_wine_detail.wine_country_id = m_wine_country.id "
						+ "INNER JOIN"
							+ " m_wine_color_type "
						+ "ON"
							+ " t_wine_detail.wine_color_type_id = m_wine_color_type.id "
						+ "INNER JOIN"
							+ " m_wine_taste_type "
						+ "ON"
							+ " t_wine_detail.wine_taste_type_id = m_wine_taste_type.id "
						+ "WHERE"
							+ " t_wine_detail.private_flg = 1 "
						+ "ORDER BY"
							+ " t_wine_detail.wine_update_date DESC";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String wineName = rs.getString("wine_name");
				int wineCreateYear = rs.getInt("wine_create_year");
				String wineGrapeName = rs.getString("wine_grape_name");
				int wineBodyScore = rs.getInt("wine_body_score");
				int wineTanninScore = rs.getInt("wine_tannin_score");
				String wineMemo = rs.getString("wine_memo");
				String wineImagePath = rs.getString("wine_image_path");
				Timestamp wineEntryDate = rs.getTimestamp("wine_create_date");
				Timestamp wineUpdateDate = rs.getTimestamp("wine_update_date");
				String wineCountry = rs.getString("w_country_name");
				String wineColor = rs.getString("w_color_type");
				String wineTaste = rs.getString("w_taste_type");

//				System.out.println(id + " set Clear");

				// nullデータ処理
				if(wineGrapeName==null) {
					wineGrapeName = "";
				}
				if(wineMemo==null) {
					wineMemo = "";
				}
				if(wineImagePath==null) {
					wineImagePath = "img/icon/no_image.jpg";
				}


				Wine wine = new Wine(id,
						wineName,
						wineCreateYear,
						wineGrapeName,
						wineBodyScore,
						wineTanninScore,
						wineMemo,
						wineImagePath,
						wineEntryDate,
						wineUpdateDate,
						wineCountry,
						wineColor,
						wineTaste);

				wineList.add(wine);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return wineList;
	}

	public List<Wine> findWaineById(int targetId) {

		// 変数初期化
		Connection conn = null;
		List<Wine> wineList = new ArrayList<Wine>();

		try {
			conn = DBManager.getConnection();

			String sql = "SELECT"
							+ " t_wine_detail.id,"
							+ " t_wine_detail.wine_name,"
							+ " t_wine_detail.wine_create_year,"
							+ " t_wine_detail.wine_grape_name,"
							+ " t_wine_detail.wine_body_score,"
							+ " t_wine_detail.wine_tannin_score,"
							+ " t_wine_detail.wine_memo,"
							+ " t_wine_detail.wine_image_path,"
							+ " t_wine_detail.wine_create_date,"
							+ " t_wine_detail.wine_update_date,"
							+ " t_wine_detail.registered_user_id,"
							+ " m_wine_country.w_country_name,"
							+ " m_wine_color_type.w_color_type,"
							+ " m_wine_taste_type.w_taste_type "
						+ "FROM"
							+ " t_wine_detail "
						+ "INNER JOIN"
							+ " m_wine_country "
						+ "ON"
							+ " t_wine_detail.wine_country_id = m_wine_country.id "
						+ "INNER JOIN"
							+ " m_wine_color_type "
						+ "ON"
							+ " t_wine_detail.wine_color_type_id = m_wine_color_type.id "
						+ "INNER JOIN"
							+ " m_wine_taste_type "
						+ "ON"
							+ " t_wine_detail.wine_taste_type_id = m_wine_taste_type.id "
						+ "WHERE"
							+ " t_wine_detail.registered_user_id = ? "
						+ "ORDER BY"
							+ " t_wine_detail.wine_update_date DESC";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, targetId);
			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				int id = rs.getInt("id");
				String wineName = rs.getString("wine_name");
				int wineCreateYear = rs.getInt("wine_create_year");
				String wineGrapeName = rs.getString("wine_grape_name");
				int wineBodyScore = rs.getInt("wine_body_score");
				int wineTanninScore = rs.getInt("wine_tannin_score");
				String wineMemo = rs.getString("wine_memo");
				String wineImagePath = rs.getString("wine_image_path");
				Timestamp wineEntryDate = rs.getTimestamp("wine_create_date");
				Timestamp wineUpdateDate = rs.getTimestamp("wine_update_date");
				String wineCountry = rs.getString("w_country_name");
				String wineColor = rs.getString("w_color_type");
				String wineTaste = rs.getString("w_taste_type");

//				System.out.println(id + " set Clear");

				// nullデータ処理
				if(wineGrapeName==null) {
					wineGrapeName = "";
				}
				if(wineMemo==null) {
					wineMemo = "";
				}
				if(wineImagePath==null) {
					wineImagePath = "img/icon/no_image.jpg";
				}


				Wine wine = new Wine(id,
						wineName,
						wineCreateYear,
						wineGrapeName,
						wineBodyScore,
						wineTanninScore,
						wineMemo,
						wineImagePath,
						wineEntryDate,
						wineUpdateDate,
						wineCountry,
						wineColor,
						wineTaste);

				wineList.add(wine);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return wineList;
	}

	/**
	 * 与えられたIDで t_wine_detail テーブルを検索
	 * @param wineId
	 * @return Wine getWineDetail
	 */
	public Wine findByWineId(int wineId) {

		//TODO 登録者と公開/非公開の項目追加

		// 変数初期化
		Connection conn = null;
		Wine getWineDetail = null;

		try {
			// コネクション確立
			conn = DBManager.getConnection();

			// SQL文
			String sql = "SELECT"
					+ " t_wine_detail.id,"
					+ " t_wine_detail.wine_name,"
					+ " t_wine_detail.wine_create_year,"
					+ " t_wine_detail.wine_grape_name,"
					+ " t_wine_detail.wine_body_score,"
					+ " t_wine_detail.wine_tannin_score,"
					+ " t_wine_detail.wine_memo,"
					+ " t_wine_detail.wine_image_path,"
					+ " t_wine_detail.wine_create_date,"
					+ " t_wine_detail.wine_update_date,"
					+ " t_wine_detail.registered_user_id,"
					+ " m_wine_country.w_country_name,"
					+ " m_wine_color_type.w_color_type,"
					+ " m_wine_taste_type.w_taste_type "
				+ "FROM"
					+ " t_wine_detail "
				+ "INNER JOIN"
					+ " m_wine_country "
				+ "ON"
					+ " t_wine_detail.wine_country_id = m_wine_country.id "
				+ "INNER JOIN"
					+ " m_wine_color_type "
				+ "ON"
					+ " t_wine_detail.wine_color_type_id = m_wine_color_type.id "
				+ "INNER JOIN"
					+ " m_wine_taste_type "
				+ "ON"
					+ " t_wine_detail.wine_taste_type_id = m_wine_taste_type.id "
				+ "WHERE"
					+ " t_wine_detail.id = ? ";

			// パラメータ設定
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, wineId);

			// 実行
			ResultSet rs = pStmt.executeQuery();

			// 一回だけ実行
			if (!rs.next()) {
				return null;
			}

			// ヒットしたデータをインスタンスにセット
			int id = rs.getInt("id");
			String wineName = rs.getString("wine_name");
			int wineCreateYear = rs.getInt("wine_create_year");
			String wineGrapeName = rs.getString("wine_grape_name");
			int wineBodyScore = rs.getInt("wine_body_score");
			int wineTanninScore = rs.getInt("wine_tannin_score");
			String wineMemo = rs.getString("wine_memo");
			String wineImagePath = rs.getString("wine_image_path");
			Timestamp wineEntryDate = rs.getTimestamp("wine_create_date");
			Timestamp wineUpdateDate = rs.getTimestamp("wine_update_date");
			String wineCountry = rs.getString("w_country_name");
			String wineColor = rs.getString("w_color_type");
			String wineTaste = rs.getString("w_taste_type");

			// nullデータ処理
			if(wineGrapeName==null) {
				wineGrapeName = "";
			}
			if(wineMemo==null) {
				wineMemo = "";
			}
			if(wineImagePath==null) {
				wineImagePath = "img/icon/no_image.jpg";
			}

			getWineDetail = new Wine(id,
					wineName,
					wineCreateYear,
					wineGrapeName,
					wineBodyScore,
					wineTanninScore,
					wineMemo,
					wineImagePath,
					wineEntryDate,
					wineUpdateDate,
					wineCountry,
					wineColor,
					wineTaste);

			return getWineDetail;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
}
