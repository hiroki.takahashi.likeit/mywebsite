package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.ColorType;
import model.Country;
import model.TasteType;

public class hoge {

	public List<Country> getCountryAll() {
		// 変数初期化
		Connection conn = null;
		List<Country> countryList = new ArrayList<Country>();

		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM m_wine_Country";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String countryName = rs.getString("w_country_name");

				Country country = new Country(id, countryName);

				countryList.add(country);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return countryList;
	}

	/**
	 * カラータイプをリスト化して返す
	 */
	public List<ColorType> getColorTypeAll() {
		// 変数初期化
		Connection conn = null;
		List<ColorType> colorTypeList = new ArrayList<ColorType>();

		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM m_wine_Color_type";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String colorTypeStr = rs.getString("w_color_type");

				ColorType colorType = new ColorType(id, colorTypeStr);

				colorTypeList.add(colorType);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return colorTypeList;
	}

	/**
	 * テイストタイプをリスト化して返す
	 */
	public List<TasteType> getTasteTypeAll() {
		// 変数初期化
		Connection conn = null;
		List<TasteType> tasteTypeList = new ArrayList<TasteType>();

		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM m_wine_taste_type";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String tasteTypeStr = rs.getString("w_taste_type");

				TasteType tasteType = new TasteType(id, tasteTypeStr);

				tasteTypeList.add(tasteType);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return tasteTypeList;
	}
}
