package model;

public class ColorType {
	private int id;
	private String colorType;

	public ColorType(int id, String colorType) {
		this.id = id;
		this.colorType = colorType;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getColorType() {
		return colorType;
	}

	public void setColorType(String colorType) {
		this.colorType = colorType;
	}

}
