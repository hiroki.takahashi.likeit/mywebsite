package model;

import java.sql.Timestamp;
import java.util.Date;

public class User {

	private int id;
	private String loginId;
	private String password;
	private String nickName;
	private Date birthDate;
	private Timestamp createDate;
	private Timestamp updateDate;

	public User(int id, String loginId, String nickName) {
		this.id = id;
		this.loginId = loginId;
		this.nickName = nickName;
	}

	public User(int id,
				String loginId,
				String password,
				String nickName,
				Date birthDate,
				Timestamp createDate,
				Timestamp updateDate) {
		this.id = id;
		this.loginId = loginId;
		this.password = password;
		this.nickName = nickName;
		this.birthDate = birthDate;
		this.createDate = createDate;
		this.updateDate = updateDate;
	}

	public User(
			String loginId,
			String password,
			String nickName,
			Date birthDate,
			Timestamp createDate,
			Timestamp updateDate) {
	this.loginId = loginId;
	this.password = password;
	this.nickName = nickName;
	this.birthDate = birthDate;
	this.createDate = createDate;
	this.updateDate = updateDate;
	}

	public User(int id) {
	this.id = id;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getnickName() {
		return nickName;
	}
	public void setnickName(String nickName) {
		this.nickName = nickName;
	}
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Timestamp getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}
	public Timestamp getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Timestamp updateDate) {
		this.updateDate = updateDate;
	}
}
