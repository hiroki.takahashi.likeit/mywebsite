package model;

import java.sql.Timestamp;

public class Wine {
	private int id;
	private String wineName;
	private int wineCreateYear;
	private String wineGrapeName;
	private int wineBodyScore;
	private int wineTanninScore;
	private String wineMemo;
	private String wineImagePath;
	private Timestamp wineEntryDate;
	private Timestamp wineUpdateDate;
	private String wineCountry;
	private String wineColor;
	private String wineTaste;
	private int registeredUserId;
	private short privateFlg;

	public Wine(int id,
			String wineName,
			int wineCreateYear,
			String wineGrapeName,
			int wineBodyScore,
			int wineTanninScore,
			String wineMemo,
			String wineImagePath,
			Timestamp wineEntryDate,
			Timestamp wineUpdateDate,
			String wineCountry,
			String wineColor,
			String wineTaste) {
		this.id = id;
		this.wineName = wineName;
		this.wineCreateYear = wineCreateYear;
		this.wineGrapeName = wineGrapeName;
		this.wineBodyScore = wineBodyScore;
		this.wineTanninScore = wineTanninScore;
		this.wineMemo = wineMemo;
		this.wineImagePath = wineImagePath;
		this.wineEntryDate = wineEntryDate;
		this.wineUpdateDate = wineUpdateDate;
		this.wineCountry = wineCountry;
		this.wineColor = wineColor;
		this.wineTaste = wineTaste;
	}

	public Wine(int id,
			String wineName,
			int wineCreateYear,
			String wineImagePath,
			Timestamp wineUpdateDate,
			String wineCountry,
			String wineColor,
			String wineTaste) {
		this.id = id;
		this.wineName = wineName;
		this.wineCreateYear = wineCreateYear;
		this.wineImagePath = wineImagePath;
		this.wineUpdateDate = wineUpdateDate;
		this.wineCountry = wineCountry;
		this.wineColor = wineColor;
		this.wineTaste = wineTaste;
	}

	public Wine(int id,
			String wineName,
			int wineCreateYear,
			String wineGrapeName,
			int wineBodyScore,
			int wineTanninScore,
			String wineMemo,
			String wineImagePath,
			Timestamp wineEntryDate,
			Timestamp wineUpdateDate,
			String wineCountry,
			String wineColor,
			String wineTaste,
			int registeredUserId,
			short privateFlg) {
		this.id = id;
		this.wineName = wineName;
		this.wineCreateYear = wineCreateYear;
		this.wineGrapeName = wineGrapeName;
		this.wineBodyScore = wineBodyScore;
		this.wineTanninScore = wineTanninScore;
		this.wineMemo = wineMemo;
		this.wineImagePath = wineImagePath;
		this.wineEntryDate = wineEntryDate;
		this.wineUpdateDate = wineUpdateDate;
		this.wineCountry = wineCountry;
		this.wineColor = wineColor;
		this.wineTaste = wineTaste;
		this.registeredUserId = registeredUserId;
		this.privateFlg = privateFlg;
	}

	public Wine(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getWineName() {
		return wineName;
	}

	public void setWineName(String wineName) {
		this.wineName = wineName;
	}

	public int getWineCreateYear() {
		return wineCreateYear;
	}

	public void setWineCreateYear(int wineCreateYear) {
		this.wineCreateYear = wineCreateYear;
	}

	public String getWineGrapeName() {
		return wineGrapeName;
	}

	public void setWineGrapeName(String wineGrapeName) {
		this.wineGrapeName = wineGrapeName;
	}

	public int getWineBodyScore() {
		return wineBodyScore;
	}

	public void setWineBodyScore(int wineBodyScore) {
		this.wineBodyScore = wineBodyScore;
	}

	public int getWineTanninScore() {
		return wineTanninScore;
	}

	public void setWineTanninScore(int wineTanninScore) {
		this.wineTanninScore = wineTanninScore;
	}

	public String getWineMemo() {
		return wineMemo;
	}

	public void setWineMemo(String wineMemo) {
		this.wineMemo = wineMemo;
	}

	public String getWineImagePath() {
		return wineImagePath;
	}

	public void setWineImagePath(String wineImagePath) {
		this.wineImagePath = wineImagePath;
	}

	public Timestamp getWineEntryDate() {
		return wineEntryDate;
	}

	public void setWineEntryDate(Timestamp wineEntryDate) {
		this.wineEntryDate = wineEntryDate;
	}

	public Timestamp getWineUpdateDate() {
		return wineUpdateDate;
	}

	public void setWineUpdateDate(Timestamp wineUpdateDate) {
		this.wineUpdateDate = wineUpdateDate;
	}

	public String getWineCountry() {
		return wineCountry;
	}

	public void setWineCountry(String wineCountry) {
		this.wineCountry = wineCountry;
	}

	public String getWineColor() {
		return wineColor;
	}

	public void setWineColor(String wineColor) {
		this.wineColor = wineColor;
	}

	public String getWineTaste() {
		return wineTaste;
	}

	public void setWineTaste(String wineTaste) {
		this.wineTaste = wineTaste;
	}

	public int getRegisteredUserId() {
		return registeredUserId;
	}

	public void setRegisteredUserId(int registeredUserId) {
		this.registeredUserId = registeredUserId;
	}

	public short getPrivateFlg() {
		return privateFlg;
	}

	public void setPrivateFlg(short privateFlg) {
		this.privateFlg = privateFlg;
	}

}
