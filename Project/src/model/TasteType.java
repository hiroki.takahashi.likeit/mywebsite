package model;

public class TasteType {
	private int id;
	private String tasteType;

	public TasteType(int id, String tasteType) {
		this.id = id;
		this.tasteType = tasteType;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTasteType() {
		return tasteType;
	}

	public void setTasteType(String tasteType) {
		this.tasteType = tasteType;
	}

}
