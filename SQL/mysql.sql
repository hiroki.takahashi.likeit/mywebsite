CREATE DATABASE mywebsite DEFAULT CHARACTER SET utf8;

USE mywebsite;

-- ユーザ情報：テーブル作成
CREATE TABLE t_user(
    id SERIAL AUTO_INCREMENT PRIMARY KEY,
    login_id varchar(255) UNIQUE NOT NULL,
    password VARCHAR(255) NOT NULL,
    user_nickname varchar(255),
    user_birth_date DATE NOT NULL,
    user_create_date DATETIME NOT NULL,
    user_update_date DATETIME NOT NULL
);

-- ユーザ情報：管理用アカウント登録
INSERT INTO
    t_user
VALUES
    (
        1,
        'admin',
        MD5('admin'),
        '管理者',
        '1980/04/01',
        sysdate(),
        sysdate()
    );

-- ワイン情報：テーブル作成
CREATE TABLE t_wine_detail(
    id SERIAL AUTO_INCREMENT PRIMARY KEY,
    wine_name VARCHAR(255) NOT NULL,
    wine_create_year INT NOT NULL,
    wine_country_id INT NOT NULL,
    wine_color_type_id INT NOT NULL,
    wine_taste_type_id INT NOT NULL,
    wine_grape_name VARCHAR(255),
    wine_body_score INT NOT NULL,
    wine_tannin_score INT NOT NULL,
    wine_memo VARCHAR(255),
    wine_image_path VARCHAR(255),
    wine_create_date DATETIME NOT NULL,
    wine_update_date DATETIME NOT NULL,
    registered_user_id int NOT NULL,
    private_flg boolean NOT NULL
);

-- ワイン情報：ダミーデータ登録
INSERT INTO t_wine_detail(
    wine_name,
    wine_create_year,
    wine_country_id,
    wine_color_type_id,
    wine_taste_type_id,
    wine_body_score,
    wine_tannin_score,
    wine_create_date,
    wine_update_date,
    registered_user_id,
    private_flg
)
VALUES(
    'ダミー',
    '1999',
    1,
    1,
    1,
    5,
    5,
    sysdate(),
    sysdate(),
    1,
    0
);


-- 生産国：テーブル作成
CREATE TABLE m_wine_country(
    id SERIAL AUTO_INCREMENT PRIMARY KEY,
    w_country_name VARCHAR(255) NOT NULL
);

-- 生産国：データ登録
INSERT INTO
    m_wine_country (id, w_country_name)
VALUES
    (1, 'フランス'),
    (2, 'イタリア'),
    (3, 'スペイン'),
    (4, 'ドイツ'),
    (5, 'アメリカ'),
    (6, 'オーストラリア'),
    (7, 'ニュージーランド'),
    (8, 'チリ'),
    (9, 'アルゼンチン'),
    (10, '日本');


-- カラータイプ：テーブル作成
CREATE TABLE m_wine_color_type(
    id SERIAL AUTO_INCREMENT PRIMARY KEY,
    w_color_type VARCHAR(255) NOT NULL
);

-- カラータイプ：データ登録
INSERT INTO
    m_wine_color_type (id, w_color_type)
VALUES
    (1, '赤'),
    (2, '白'),
    (3, 'ロゼ');


-- 味タイプ：テーブル作成
CREATE TABLE m_wine_taste_type(
    id SERIAL AUTO_INCREMENT PRIMARY KEY,
    w_taste_type VARCHAR(255) NOT NULL
);

-- 味タイプ：データ登録
INSERT INTO
    m_wine_taste_type (id, w_taste_type)
VALUES
    (1, '辛口'),
    (2, '甘口'),
    (3, '極甘口');
